package com.ksj.newsscrap.controller;

import com.ksj.newsscrap.model.MoneyChangeRequest;
import com.ksj.newsscrap.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController // API 를 만드는 콘트롤러
@RequestMapping("/money")
@RequiredArgsConstructor // 필수 인수 생성자
// class - 내용들 을 담을 공간, TempController - 파일명과 클래스명은 동일하다
public class MoneyController { // 이름이 Temp 가 아닌 TempController 인 이유 - 찾기 쉽도록

    // @RequiredArgsConstructor 이 달리면서 혼자 작동할 수 없고 협업을 하도록 설정됨
    // private - 숨긴다, final - 다 올때까지 기다린다, MoneyService - 공간, moneyService - 이름
    private final MoneyService moneyService;

    // class 안에 있는 사람
    @PostMapping("/change")
    // peopleChange - 이름은 알아보기 쉽게 가독성을 생각해서 정한다
    // public - 접근 제어자, String - 돌려줄 타입, peopleChange - 이름, () - 바구니
    public String peopleChange(@RequestBody MoneyChangeRequest request) { // {} - 메소드(함수), { - 내용의 시작, } - 내용의 끝
        String result = moneyService.convertMoney(request.getMoney());
        return result;
    }

    // class 안에 있는 사람
    @GetMapping("/pay-back") // 명판의 바구니 안에는 케밥 케이스 로 적는다
    public String peoplePayback() { // String - peoplePayback 에게서 돌려받을 자료의 타입은 문자열 이다
        return "환불되었습니다. 고객님";
    }
}
