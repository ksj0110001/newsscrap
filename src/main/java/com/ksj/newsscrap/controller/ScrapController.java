package com.ksj.newsscrap.controller;

import com.ksj.newsscrap.model.NewsItem;
import com.ksj.newsscrap.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor // 혼자 일 못하고 협업을 한다
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService; // 협업을 하는 대상, 나만 알아야 하므로 다른사람 에게는 숨긴다

    @GetMapping("/html")
    // 팀장이 List<NewsItem> 타입의 자료를 주겠다고 했기 때문에 같은 타입로 받는다, 오류 발생시 던지고 탈출한다
    public List<NewsItem> getHtml() throws IOException {
        List<NewsItem> result = scrapService.run(); // List<NewsItem> 타입의 결과물을 서비스 팀장 에게 받아서 result 에 담는다
        return result; // List<NewsItem> 타입의 result 를 돌려준다
    }
}
