package com.ksj.newsscrap.model;

import lombok.Getter;
import lombok.Setter;

@Getter // 가져다 주는 사람, 모델 에서 보안을 위해 쓰인다
@Setter // 세팅 해 주는 사람, 모델 에서 보안을 위해 쓰인다
public class MoneyChangeRequest { // Request - 요청하다
    // private - 숨김, Integer - model 에서는 가독성을 위해 래퍼 클래스 로 적는다
    private Integer money;
}
