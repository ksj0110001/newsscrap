package com.ksj.newsscrap.service;

import com.ksj.newsscrap.model.NewsItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException { // 팀원 1 (Document 를 가져온다), 오류 발생시 던지고 탈출한다
        String url = "http://www.astronomer.rocks/"; // 접속할 주소를 String 타입의 url 이라는 박스에 담는다
        Connection connection = Jsoup.connect(url); // Jsoup 기능을 사용해서 (url) 에 접속한 상태로 connection 에 담긴다

        Document document = connection.get(); // connection 에 담긴것이 document 에 담긴다

        return document; // Jsoup 기능을 사용해서 (url) 에 접속한 상태 인 document 를 팀장 에게 돌려준다
    }

    private List<Element> parseHtml(Document document) { // 팀원 2 (파싱한다), document 를 받아서 작업한 후 List<Element> 에 넣어서 주겠다
        // document 에서 Class 안에 있는 요소들 중 auto-article 이 포함된 것들만 파싱 해서 모두 가져온 후 elements 에 담는다
        Elements elements = document.getElementsByClass("auto-article");

        // 연결된 리스트 형태로 결과를 담기위해 만든 빈 공간 (LinkedList<>()) 의 이름은 tempResult 이다
        List<Element> tempResult = new LinkedList<>();

        for (Element item : elements) { // elements 가 다 소진될 때 까지 하나씩 반복해서 파싱한 후 item 에 담는다
            // item 에서 Tag 안에 있는 요소들 중 li 가 포함된 것들만 파싱 해서 lis 에 담는다, 여러개 나오는 경우도 있으니 복수 Elements lis
            Elements lis = item.getElementsByTag("li");
            for (Element item2 :lis) { // lis 가 다 소진될 때 까지 하나씩 반복해서 파싱 해서 item2 에 담는다
                tempResult.add(item2); // item2 에 담긴 자료를 tempResult 에 더해준다
            }
        }

        return tempResult; // List<Element> 타입의 tempResult 를 팀장에게 돌려준다
    }

    // List<Element> list 를 가공해서 담을 공간 NewsItem 을 model package 에 만든다
    private List<NewsItem> makeResult(List<Element> list) { // 팀원 3 (예쁘게 가공해서 담아준다)
        // 연결된 리스트 형태로 결과를 담기위해 만든 빈 공간 (LinkedList<>()) 의 이름은 result 이다
        List<NewsItem> result = new LinkedList<>();

        for (Element item : list) { // list 가 다 소진될 때 까지 하나씩 반복해서 파싱한 후 item 에 담는다
            // item 에서 Class 안에 있는 요소들 중 flow-hidden 이 포함된 것들만 파싱 해서 가져온 후 checkContents 에 담는다
            Elements checkContents = item.getElementsByClass("flow-hidden");
            if (checkContents.size() == 0) { // 만약 flow-hidden 을 찾은 결과값이 0 이면 (없으면) 가공한다
                // item 에서 Class 안에 있는 요소들 중 auto-fontB 가 포함된 것들만 파싱 해서 가져온 후 checkHiddenBanner 에 담는다
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB");
                if (checkHiddenBanner.size() > 0) { // 만약 auto-fontB 를 찾은 결과값이 0 보다 크면 (있으면) 가공한다
                    // item 에서 Class 안에 있는 요소들 중 auto-titles 가 있는지 확인 후 0번(첫번째) auto-titles 을 가져온 후
                    // Tag 안에 있는 요소들 중 strong 이 있는지 확인 후 0번(첫번째) 에 있는 text 를 가져와서 title 에 담는다
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    // item 에서 Class 안에 있는 요소들 중 auto-fontB 가 있는지 확인 후 0번(첫번째) 에 있는 text 를 가져와서 content 에 담는다
                    String content = item.getElementsByClass("auto-fontB").get(0).text();

                    NewsItem addItem = new NewsItem(); // 결과물을 담기위해 만든 빈 박스 (NewsItem()) 의 이름은 addItem 이다
                    addItem.setTitle(title); // title 을 model package 에 만들어둔 NewsItem 의 title 항목에 넣는다
                    addItem.setContent(content); // content 를 model package 에 만들어둔 NewsItem 의 content 항목에 넣는다

                    result.add(addItem); // addItem 에 담긴 자료를 result 에 더해준다
                }
            }
        }

        return result; // List<NewsItem> 타입의 result 를 팀장에게 돌려준다
    }

    public List<NewsItem> run() throws IOException { // 팀장 (작업을 지시하고 결과물을 취합한다), 오류 발생시 던지고 탈출한다
        Document document = getFullHtml(); // 팀원 1 의 결과물을 받는다
        List<Element> elements = parseHtml(document); // 팀원 2 의 결과물을 받는다
        List<NewsItem> result = makeResult(elements); // 팀원 3 의 결과물을 받는다

        return result; // List<NewsItem> 타입의 result 를 controller 에게 돌려준다
    }
}
