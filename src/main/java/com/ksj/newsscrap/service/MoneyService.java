package com.ksj.newsscrap.service;

import org.springframework.stereotype.Service;

@Service // 실제 Service 기능을 하는곳
public class MoneyService {

    // public - 공개한다, convertMoney 가 (int realMoney) 를 받아서 String 형태로 돌려준다
    public String convertMoney(int realMoney) {
        return "상품권 " + realMoney;
    }
}
